import './App.css';
import axios from "axios";
import { useEffect, useState } from 'react';

function App() {
  const [countries, setCountries] = useState([]);

  const fetchData = async () => {
    const res = await axios.get("https://restcountries.com/v3.1/all")
    return res.data;
  }

  const handleClick = async () => {
    const data = await fetchData();
    setCountries(data);
  }

  // some changes

  return (
    <div className="App">
      <button
        onClick = {handleClick}
      >Get</button>
      {
        countries.length > 0 && countries.map(country => {
          return <p key = {country.name.common}>{country.name.common}</p>
        })
      }
    </div>
  );
}

export default App;
